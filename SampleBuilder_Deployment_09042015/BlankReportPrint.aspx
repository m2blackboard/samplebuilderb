﻿<%@ page language="C#" autoeventwireup="true" inherits="BlankReportPrint, App_Web_z4pecfdp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link href="css/Sample.css?v=1" rel="stylesheet" type="text/css" />
    <title>Untitled Page</title>
     <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
     <script type="text/javascript">
     $(document).ready(function () {
        
        PrintWindow();
        });
        function PrintWindow() {
            window.print();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <br/>
    <h1 class="PageHeader">
       <span class="insetText"><span class="blueTITLEtext">&nbsp;<asp:Literal ID="litProjectName" runat="server"></asp:Literal></span> </span></h1>
       <br />
       <table width="100%" border="0">
    <tr><td colspan="2" style="text-align:right" valign="top">
    <%--<div><img src="Images/print4.gif" alt="" />
                            <a id="lnkprint" style="vertical-align:top;" target="_blank" runat="server" href="#">Print</a>--%>
                            <%--<img src="Images/Export2.jpg" alt="" />
                            <a href="#" id="lnkExport" style="vertical-align:top;" runat="server" enableviewstate="false" class="clsSmallColorText" >Export</a>--%>
    <%--                        </div>--%>
</td></tr>
    <tr>
    <td style="width:40%;text-align:left;vertical-align:top">
    <asp:Literal ID="litNotes" runat="server"></asp:Literal>
    </td>
    <td style="width:50%;text-align:right" valign="top">
    <table width="100%" border="0">
    <tr>
    <td style="text-align:left;line-height:40px;" class="clsSubText">
    Project Details:
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Interviewer:&nbsp;&nbsp;&nbsp;<asp:Literal ID="litInterviewer" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Supervisor:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitSupervisor" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Area:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitArea" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Rate:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitRate" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Job Number:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitJobNumber" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Contact Person:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitContactPerson" runat="server"></asp:Literal>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <%--<tr>
    <td colspan="2">
    <br />
    </td>
    </tr>--%>
    </table>
    <br />
    <asp:Literal ID="litSummary" runat="server"></asp:Literal>
    <br />
    <div style="bottom:0;position:fixed;text-align:center;width:100%">
    <asp:Literal ID="litFooter" runat="server"></asp:Literal>
    </div>
    </div>
    </form>
</body>
</html>
