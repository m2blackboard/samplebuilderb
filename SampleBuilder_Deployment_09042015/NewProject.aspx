﻿<%@ page language="C#" masterpagefile="~/SampleMaster.master" autoeventwireup="true" inherits="NewProject, App_Web_dvy2uzvm" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/SampleMaster.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 15%;
            height: 545px;
        }
        .style2
        {
            height: 545px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" >
    </asp:ScriptManager>

<%--<div style="margin-left:10px;margin-right:10px;">
        <table cellpadding="0" cellspacing="0" border="0" style="" width="100%">
        <tr>
        <td class="SuccessLeft"></td>
        <td class="SuccessTile"><div class="errorText">Testing</div></td>
        <td class="SuccessRight"></td>
        </tr>
        </table>
        </div>--%>
    <h1 class="PageHeader">
       <span class="insetText"> <span class="blueTITLEtext">&nbsp;Create Project</span> </span></h1>
       <br />
       <asp:Panel ID="pnlStep1" Visible="true" runat="server">
       <table cellpadding="0" cellspacing="0" width="100%">
    <%--<tr>
    <td colspan="2"><br /></td>
    </tr>--%>
    <tr>
    <td colspan="2">
    <span class="blueTITLEtext" style="font-size:medium">Step1: Project Details</span> 
    <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
    </td>
    </tr>
    <tr>
    <td style="width:15%">
    Project Name:
    </td>
    <td>
        <asp:TextBox ID="TxtProject" Width="300px" BorderStyle="Ridge" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ControlToValidate="TxtProject" ErrorMessage="Please Enter Project Name">*</asp:RequiredFieldValidator>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>Sample Type: </td>
    <td>
        <asp:DropDownList ID="drpProjectSampleType" runat="server" AutoPostBack="true"  onselectedindexchanged="drpProjectSampleType_SelectedIndexChanged">
        <asp:ListItem Text="Select" Value="0"></asp:ListItem>
        <asp:ListItem Text="Standard" Value="2"></asp:ListItem>
        <%--<asp:ListItem Text="InterLocked" Value="1"></asp:ListItem> --%>
        </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr><td colspan="2">
    <table id="tblInterlock" runat="server" cellpadding="0" cellspacing="0" visible="false" width="100%">
    <tr>
    <td style ="width:15%">Base Project:</td>
    <td>
        <asp:DropDownList ID="DrpBaseProject" runat="server" AutoPostBack="true"  onselectedindexchanged="DrpBaseProject_SelectedIndexChanged">
        </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    </table>
    </td></tr>
    <tr>
    <td>
    Job Number:
    </td>
    <td>
        <asp:TextBox ID="TxtJobNumber" Width="300px" BorderStyle="Ridge" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ControlToValidate="TxtJobNumber" ErrorMessage="Please Enter Job Number">*</asp:RequiredFieldValidator>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Interviewer:
    </td>
    <td>
        <asp:TextBox ID="TxtInterviewer"  Width="300px" BorderStyle="Ridge" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
     <tr>
    <td>
    Contact Person:
    </td>
    <td>
        <asp:DropDownList ID="drpContactPerson" runat="server">
        <%--<asp:ListItem Text="Select" Value="0"></asp:ListItem>
        <asp:ListItem Text="Bhagya" Value="1"></asp:ListItem>
        <asp:ListItem Text="Laxmi" Value="2"></asp:ListItem>--%>
        </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Supervisor:
    </td>
    <td>
        <asp:TextBox ID="TxtSupervisor" Width="300px" BorderStyle="Ridge" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Area:
    </td>
    <td>
        <asp:TextBox ID="TxtArea" Width="300px" BorderStyle="Ridge" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Rate:
    </td>
    <td>
    <div style="height:100px;overflow-y:scroll;border:1px solid black;width:220px;">
        <asp:GridView ID="gdRateCodes" Width="200px" Height="100"  AutoGenerateColumns="false" runat="server" >
        <Columns>
        <asp:TemplateField>
        <HeaderStyle  BackColor="#CCCCFF" />
        <ItemTemplate>
        <asp:CheckBox id="chkSelRateCodes" runat="server" />
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-CssClass="clsLeft" HeaderStyle-BackColor="#CCCCFF">
        <HeaderTemplate>
        Select RateCodes
        </HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="LblRateCode" Text='<%# Bind("ratecode") %>'  runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-CssClass="clsWidth" Visible="false">
        <HeaderStyle BackColor="#CCCCFF" />
        <ItemTemplate >
        <asp:Label ID="LblRateCodeId" Text='<%# Bind("ratecodeid") %>' Visible="true" runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        </Columns>
        <RowStyle BackColor="" />
        <AlternatingRowStyle  BackColor=""/>
        </asp:GridView>
        </div>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>Project Type: </td>
    <td>
        <asp:DropDownList ID="DrpSampleType" runat="server">
        <asp:ListItem Text="Link" Value="Link"></asp:ListItem>
        <asp:ListItem Text="Adhoc" Value="Adhoc"></asp:ListItem>
        <asp:ListItem Text="ATP" Value="ATP"></asp:ListItem>
        </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>Project Methodology</td>
    <td>
        <asp:DropDownList ID="DrpProjectType" runat="server">
        <asp:ListItem Text="PAPI" Value="PAPI"></asp:ListItem>
        <asp:ListItem Text="CAPI" Value="CAPI"></asp:ListItem>
        </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>Week Number: </td>
    <td>
        <asp:DropDownList ID="DrpWeekNumber" runat="server">
        <asp:ListItem Text="1" Value="1"></asp:ListItem>
        <asp:ListItem Text="2" Value="2"></asp:ListItem>
        <asp:ListItem Text="3" Value="3"></asp:ListItem>
        <asp:ListItem Text="4" Value="4"></asp:ListItem>
        <asp:ListItem Text="5" Value="5"></asp:ListItem>
        <asp:ListItem Text="6" Value="6"></asp:ListItem>
        <asp:ListItem Text="7" Value="7"></asp:ListItem>
        <asp:ListItem Text="8" Value="8"></asp:ListItem>
        <asp:ListItem Text="9" Value="9"></asp:ListItem>
        <asp:ListItem Text="10" Value="10"></asp:ListItem>
        <asp:ListItem Text="11" Value="11"></asp:ListItem>
        <asp:ListItem Text="12" Value="12"></asp:ListItem>
        <asp:ListItem Text="13" Value="13"></asp:ListItem>
        <asp:ListItem Text="14" Value="14"></asp:ListItem>
        <asp:ListItem Text="15" Value="15"></asp:ListItem>
        <asp:ListItem Text="16" Value="16"></asp:ListItem>
        <asp:ListItem Text="17" Value="17"></asp:ListItem>
        <asp:ListItem Text="18" Value="18"></asp:ListItem>
        <asp:ListItem Text="19" Value="19"></asp:ListItem>
        <asp:ListItem Text="20" Value="20"></asp:ListItem>
        <asp:ListItem Text="21" Value="21"></asp:ListItem>
        <asp:ListItem Text="22" Value="22"></asp:ListItem>
        <asp:ListItem Text="23" Value="23"></asp:ListItem>
        <asp:ListItem Text="24" Value="24"></asp:ListItem>
        <asp:ListItem Text="25" Value="25"></asp:ListItem>
        <asp:ListItem Text="26" Value="26"></asp:ListItem>
        <asp:ListItem Text="27" Value="27"></asp:ListItem>
        <asp:ListItem Text="28" Value="28"></asp:ListItem>
        <asp:ListItem Text="29" Value="29"></asp:ListItem>
        <asp:ListItem Text="30" Value="30"></asp:ListItem>
        <asp:ListItem Text="31" Value="31"></asp:ListItem>
        <asp:ListItem Text="32" Value="32"></asp:ListItem>
        <asp:ListItem Text="33" Value="33"></asp:ListItem>
        <asp:ListItem Text="34" Value="34"></asp:ListItem>
        <asp:ListItem Text="35" Value="35"></asp:ListItem>
        <asp:ListItem Text="36" Value="36"></asp:ListItem>
        <asp:ListItem Text="37" Value="37"></asp:ListItem>
        <asp:ListItem Text="38" Value="38"></asp:ListItem>
        <asp:ListItem Text="39" Value="39"></asp:ListItem>
        <asp:ListItem Text="40" Value="40"></asp:ListItem>
        <asp:ListItem Text="41" Value="41"></asp:ListItem>
        <asp:ListItem Text="42" Value="42"></asp:ListItem>
        <asp:ListItem Text="43" Value="43"></asp:ListItem>
        <asp:ListItem Text="44" Value="44"></asp:ListItem>
        <asp:ListItem Text="45" Value="45"></asp:ListItem>
        <asp:ListItem Text="46" Value="46"></asp:ListItem>
        <asp:ListItem Text="47" Value="47"></asp:ListItem>
        <asp:ListItem Text="48" Value="48"></asp:ListItem>
        <asp:ListItem Text="49" Value="49"></asp:ListItem>
        <asp:ListItem Text="50" Value="50"></asp:ListItem>
        <asp:ListItem Text="51" Value="51"></asp:ListItem>
        <asp:ListItem Text="52" Value="52"></asp:ListItem>
        </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>Briefing/ into Field:</td>
    <td>
    <asp:TextBox ID="TxtBreifing" runat="server" ></asp:TextBox>
    <cc1:CalendarExtender ID="CalendarExtender1" Format="yyyy-MM-dd" TargetControlID="TxtBreifing" runat="server">
    </cc1:CalendarExtender>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
            ControlToValidate="TxtBreifing" ErrorMessage="Enter Briefing">*</asp:RequiredFieldValidator>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>Due in Rivonia Locals:</td>
    <td>
    <asp:TextBox ID="TxtDueInLocal" runat="server" ></asp:TextBox>
    <cc1:CalendarExtender ID="CalendarExtender2" Format="yyyy-MM-dd" TargetControlID="TxtDueInLocal" runat="server">
    </cc1:CalendarExtender>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
            ControlToValidate="TxtDueInLocal" ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
      <asp:CompareValidator ID="CompareValidator1" runat="server" 
            ControlToCompare="TxtBreifing" ControlToValidate="TxtDueInLocal" 
            ErrorMessage="Due in Rivonia Locals should be greater then Briefing/ into Field" Operator="GreaterThan" Type="Date"></asp:CompareValidator>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>Due in Rivonia Area:</td>
    <td>
    <asp:TextBox ID="TxtDueInArea" runat="server" ></asp:TextBox>
    <cc1:CalendarExtender ID="CalendarExtender3" Format="yyyy-MM-dd" TargetControlID="TxtDueInArea" runat="server">
    </cc1:CalendarExtender>
    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
            ControlToValidate="TxtDueInArea" ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
      <asp:CompareValidator ID="CompareValidator2" runat="server" 
            ControlToCompare="TxtBreifing" ControlToValidate="TxtDueInArea" 
            ErrorMessage="Due in Rivonia Area should be greater then Briefing/ into Field" Operator="GreaterThan" Type="Date"></asp:CompareValidator>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td></td>
    <td><asp:CheckBox ID="ChkInterLock" runat="server" Visible="false" Text="Is it interlocked sample?" 
            oncheckedchanged="ChkInterLock_CheckedChanged" AutoPostBack="true" /></td>
    </tr>
    <tr>
    <td colspan="2">
    <asp:Panel ID="PnlInterLock" runat="server" Visible="false">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    <tr>
    <td style="width:15%">Interlock Sample Type</td>
    <td><asp:DropDownList ID="drpSample" runat="server">
    <asp:ListItem Text="Total" Value="0"></asp:ListItem>
    <asp:ListItem Text="Black" Value="1"></asp:ListItem>
    <asp:ListItem Text="White" Value="2"></asp:ListItem>
    <asp:ListItem Text="Coloured" Value="3"></asp:ListItem>
    <asp:ListItem Text="Indian" Value="4"></asp:ListItem>
    </asp:DropDownList></td>
    </tr>
    </table>
    </asp:Panel>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
    </td>
    </tr>
    <tr>
    <td >
    </td>
    <td>
    <asp:Button ID="btnStep1Next" Text="Next" CssClass="clsButton"  runat="server" 
            onclick="btnStep1Next_Click" />
    </td>
    </tr>
    </table>
    </asp:Panel>
    <asp:Panel ID="PnlStep2" Visible="false" runat="server">
   <table cellpadding="0" cellspacing="0" width="100%">
   <tr>
    <td colspan="2">
    <span class="blueTITLEtext" style="font-size:medium">Step2:  Select Locations</span> 
    <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td style="width:15%" valign="top">
    Select Locations:
    </td>
    <td>
        <asp:GridView ID="gdLocations" Width="200px" Height="200px"  AutoGenerateColumns="false" runat="server">
					<Columns>
						<asp:TemplateField>
							<HeaderStyle  BackColor="#CCCCFF" />
							<HeaderTemplate>
								<asp:CheckBox ID="ChkLocSelectAll" AutoPostBack="true" OnCheckedChanged="ChkLocSelectAll_CheckedChanged" runat="server" />
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox id="chkSelLoc" runat="server" />
							</ItemTemplate>
						</asp:TemplateField>
						<%--<asp:BoundField DataField="locname" />--%>
						<asp:TemplateField HeaderStyle-CssClass="clsLeft" HeaderStyle-BackColor="#CCCCFF">
							<HeaderTemplate>
							Select Locations
							</HeaderTemplate>
							<ItemTemplate>
								<asp:Label ID="LblLocationName" Text='<%# Bind("locname") %>'  runat="server"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderStyle-CssClass="clsWidth" Visible="true">
							<HeaderStyle BackColor="#CCCCFF" />
							<ItemTemplate >
								<asp:Label ID="LblLocationId" Text='<%# Bind("locid") %>' Visible="true" runat="server"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="clsWidth" Visible="true">
							<HeaderStyle BackColor="#CCCCFF" />
							<HeaderTemplate>
								Black
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox id="chkSelLocEthBlack" Checked='<%# Bind("EthnicityBlack") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="clsWidth" Visible="true">
							<HeaderStyle BackColor="#CCCCFF" />
							<HeaderTemplate>
								Coloured
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox Width="40px" id="chkSelLocEthColoured" Checked='<%# Bind("EthnicityColoured") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="clsWidth" Visible="true">
							<HeaderStyle BackColor="#CCCCFF" />
							<HeaderTemplate>
								Indian
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox id="chkSelLocEthIndian" Checked='<%# Bind("EthnicityIndian") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="clsWidth" Visible="true">
							<HeaderStyle BackColor="#CCCCFF" />
							<HeaderTemplate>
								White
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox id="chkSelLocEthWhite" Checked='<%# Bind("EthnicityWhite") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
					<RowStyle BackColor="" />
					<AlternatingRowStyle  BackColor=""/>
        </asp:GridView>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td colspan="2">
    <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
    </td>
    </tr>
        <tr>
        <td></td>
    <td >
    <asp:Button ID="BtnStep2Back" Text="Back" CssClass="clsButton" runat="server" 
            onclick="BtnStep2Back_Click" />&nbsp;&nbsp;&nbsp;
    <asp:Button ID="BtnStep2" Text="Next" runat="server" CssClass="clsButton" onclick="BtnStep2_Click" />
    </td>
    </tr>
    </table>    
    </asp:Panel>
   <asp:Panel ID="PnlLocOrder" Visible="false" runat="server">
   <table cellpadding="0" cellspacing="0" width="100%">
   <tr>
    <td colspan="2">
    <span class="blueTITLEtext" style="font-size:medium">Step3:  Reorder Locations</span> 
    <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td style="width:15%" valign="top">
    Selected Locations:
    </td>
    <td>
        <asp:GridView ID="gdLocReorder" Width="200px"  AutoGenerateColumns="false" runat="server" OnRowDataBound="grdLocOrder_RowDataBound">
        <Columns>
        <asp:TemplateField HeaderStyle-CssClass="clsLeft" HeaderStyle-BackColor="#CCCCFF">
        <HeaderTemplate>
            Selected Locations
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Label ID="LblLocationName" Text='<%# Bind("locname") %>'  runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-CssClass="clsLeft" HeaderStyle-BackColor="#CCCCFF">
        <HeaderTemplate>
            Sort
        </HeaderTemplate>
        <ItemTemplate>
            <asp:DropDownList ID="drpSort" runat="server" Width="100%">
        </asp:DropDownList>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-CssClass="clsWidth" Visible="false">
        <HeaderStyle BackColor="#CCCCFF" />
        <ItemTemplate >
            <asp:Label ID="LblLocationId" Text='<%# Bind("locid") %>' Visible="true" runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        </Columns>
        <RowStyle BackColor="" />
        <AlternatingRowStyle  BackColor=""/>
        </asp:GridView>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td colspan="2">
    <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
    </td>
    </tr>
        <tr>
        <td></td>
    <td >
    <asp:Button ID="BtnOrderBack" Text="Back" CssClass="clsButton" runat="server" 
            onclick="BtnOrderBack_Click" />&nbsp;&nbsp;&nbsp;
    <asp:Button ID="BtnOrderNext" Text="Next" runat="server" CssClass="clsButton" 
            onclick="BtnOrderNext_Click" />
    </td>
    </tr>
    </table>    
    </asp:Panel>
    <asp:Panel ID="PnlStep3" Visible="false" runat="server">
   <table cellpadding="0" cellspacing="0" width="100%">
   <tr>
    <td colspan="2">
    <span class="blueTITLEtext" style="font-size:medium">Step4:  Select Interviewers</span> 
    <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td style="width:15%" valign="top">
    Select Location:
    </td>
    <td>
        <asp:DropDownList ID="drpStep3Location" runat="server" AutoPostBack="true"
            onselectedindexchanged="drpStep3Location_SelectedIndexChanged">
        <%--<asp:ListItem Text="Johannesburg" Value="1"></asp:ListItem>
        <asp:ListItem Text="Pretoria" Value="2"></asp:ListItem>
        <asp:ListItem Text="Capetown" Value="3"></asp:ListItem>--%>
        </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td valign="top" class="style1">
    Select Interviewers:
    </td>
    <td class="style2" valign="top">
        <asp:GridView ID="GdInterviewer" Width="300px"   AutoGenerateColumns="false" runat="server">
        <Columns>
        <asp:TemplateField>
        <HeaderStyle  BackColor="#CCCCFF" />
        <HeaderTemplate>
        <asp:CheckBox ID="ChkInterSelectAll" AutoPostBack="true" runat="server" OnCheckedChanged="ChkInterSelectAll_CheckedChanged" />
        </HeaderTemplate>
        <ItemTemplate>
        <asp:CheckBox id="chkSelInterviewer" runat="server" />
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-CssClass="clsLeft">
        <HeaderStyle  BackColor="#CCCCFF" />
        <HeaderTemplate >Interviewer Id</HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="LblInterviewerSysId" Text='<%# Bind("interviewersysid") %>'  runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-CssClass="clsLeft">
        <HeaderStyle  BackColor="#CCCCFF" />
        <HeaderTemplate >Select Interviewers</HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="LblInterviewerName" Text='<%# Bind("interviewername") %>'  runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-CssClass="clsLeft" Visible="false">
        <HeaderStyle  BackColor="#CCCCFF" />
        <ItemTemplate>
        <asp:Label ID="LblInterviewerId" Text='<%# Bind("interviewerautoid") %>' Visible="true" runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        </Columns>
        </asp:GridView>
        <br />
        <asp:Button ID="BtnAddInterviewer" Text="Add"  runat="server" CssClass="clsButton" onclick="BtnAddInterviewer_Click" />
            <br />
            <br />
            <asp:GridView ID="GDTran" Width="500px"   AutoGenerateColumns="false" runat="server" AutoGenerateDeleteButton="true" OnRowDeleting="GDTran_RowDeleting">
            <Columns>
            <asp:TemplateField HeaderStyle-CssClass="clsLeft" Visible="false">
            <HeaderStyle  BackColor="#CCCCFF" />
            <ItemTemplate>
            <asp:Label ID="LblLocId" runat="server" Visible="true" Text='<%# Bind("locid") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-CssClass="clsLeft">
            <HeaderStyle  BackColor="#CCCCFF" />
            <HeaderTemplate>Location</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID="LblLocName" runat="server"  Text='<%# Bind("locName") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-CssClass="clsLeft">
            <HeaderStyle  BackColor="#CCCCFF" />
            <HeaderTemplate>Interviewers</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID="LblInterviewerName" runat="server" Text='<%# Bind("InterviewersName") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-CssClass="clsLeft" Visible="false">
            <HeaderStyle  BackColor="#CCCCFF" />
            <ItemTemplate>
            <asp:Label ID="LblInterviewerId" Visible="true" runat="server" Text='<%# Bind("InterviewerIds") %>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="locid" />
            <asp:BoundField DataField="locName" />
            <asp:BoundField DataField="InterviewersName" />
            <asp:BoundField DataField="InterviewerIds" />--%>
            </Columns>
        </asp:GridView>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td colspan="2">
    <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
    </td>
    </tr>
        <tr>
        <td></td>
    <td >
    <asp:Button ID="BtnStep3Back" Text="Back" runat="server" CssClass="clsButton" onclick="BtnStep3Back_Click"  />&nbsp;&nbsp;&nbsp;
    <asp:Button ID="BtnStep3" Text="Next" runat="server" CssClass="clsButton" onclick="BtnStep3_Click" />    
    </td>
    </tr>
    </table>    
    </asp:Panel>
    <asp:Panel ID="PnlStep4" Visible="false" runat="server">
   <table cellpadding="0" cellspacing="0" width="100%">
   <tr>
    <td colspan="2">
    <span class="blueTITLEtext" style="font-size:medium">Step5:  Select File to Export</span> 
    <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>    
    <tr>
    <td valign="top">Summary: </td>
    <td>
    <div class="clsSubText" style="line-height:40px">
            All Respondents Must:
            </div>
        <asp:TextBox ID="TxtSummary" TextMode="MultiLine" Width="400px" BorderStyle="Ridge" Height="300px" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
            ControlToValidate="TxtSummary" ErrorMessage="">*</asp:RequiredFieldValidator>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Select File To Import:
    </td>
    <td>
        <asp:FileUpload ID="FvUpload" runat="server" BorderStyle="Ridge" />
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td colspan="2">
    <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
    </td>
    </tr>
        <tr>
        <td></td>
    <td>
    <asp:Button ID="BtnBack" Text="Back" runat="server" onclick="BtnBack_Click" CssClass="clsButton" CausesValidation="false" />&nbsp;&nbsp;&nbsp;
    <asp:Button ID="BtnFinish" Text="Finish" runat="server" onclick="BtnFinish_Click" CssClass="clsButton" />
    </td>
    </tr>
    </table>    
    </asp:Panel>
</asp:Content>

