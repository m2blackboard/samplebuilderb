﻿<%@ page language="C#" masterpagefile="~/SampleMaster.master" autoeventwireup="true" inherits="SampleNew, App_Web_dvy2uzvm" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/SampleMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br/>
<br/>
    <h1 class="PageHeader">
       <span class="insetText"> <span class="blueTITLEtext">&nbsp;Create Project</span> </span></h1>
       <br />
       <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td style="width:15%">
    Project Name:
    </td>
    <td>
        <asp:TextBox ID="TxtProject" Width="300px" BorderStyle="Ridge" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Interviewer:
    </td>
    <td>
        <asp:TextBox ID="TxtInterviewer"  Width="300px" BorderStyle="Ridge" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
     <tr>
    <td>
    Contact Person:
    </td>
    <td>
        <asp:TextBox ID="TxtContactPerson" Width="300px" BorderStyle="Ridge" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Supervisor:
    </td>
    <td>
        <asp:TextBox ID="TxtSupervisor" Width="300px" BorderStyle="Ridge" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Area:
    </td>
    <td>
        <asp:TextBox ID="TxtArea" Width="300px" BorderStyle="Ridge" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Rate:
    </td>
    <td>
        <asp:TextBox ID="TxtRate" Width="300px" BorderStyle="Ridge" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Job Number:
    </td>
    <td>
        <asp:TextBox ID="TxtJobNumber" Width="300px" BorderStyle="Ridge" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>Sample Type: </td>
    <td>
        <asp:DropDownList ID="DrpSampleType" runat="server">
        <asp:ListItem Text="Link" Value="Link"></asp:ListItem>
        <asp:ListItem Text="Adhoc" Value="Adhoc"></asp:ListItem>
        <asp:ListItem Text="ATP" Value="ATP"></asp:ListItem>
        </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>Project Type: </td>
    <td>
        <asp:DropDownList ID="DrpProjectType" runat="server">
        <asp:ListItem Text="PAPI" Value="PAPI"></asp:ListItem>
        <asp:ListItem Text="CAPI" Value="CAPI"></asp:ListItem>
        </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td>Week Number: </td>
    <td>
        <asp:DropDownList ID="DrpWeekNumber" runat="server">
        <asp:ListItem Text="1" Value="1"></asp:ListItem>
        <asp:ListItem Text="2" Value="2"></asp:ListItem>
        <asp:ListItem Text="3" Value="3"></asp:ListItem>
        <asp:ListItem Text="4" Value="4"></asp:ListItem>
        <asp:ListItem Text="5" Value="5"></asp:ListItem>
        <asp:ListItem Text="6" Value="6"></asp:ListItem>
        <asp:ListItem Text="7" Value="7"></asp:ListItem>
        <asp:ListItem Text="8" Value="8"></asp:ListItem>
        <asp:ListItem Text="9" Value="9"></asp:ListItem>
        <asp:ListItem Text="10" Value="10"></asp:ListItem>
        <asp:ListItem Text="11" Value="11"></asp:ListItem>
        <asp:ListItem Text="12" Value="12"></asp:ListItem>
        <asp:ListItem Text="13" Value="13"></asp:ListItem>
        <asp:ListItem Text="14" Value="14"></asp:ListItem>
        <asp:ListItem Text="15" Value="15"></asp:ListItem>
        <asp:ListItem Text="16" Value="8"></asp:ListItem>
        <asp:ListItem Text="17" Value="8"></asp:ListItem>
        <asp:ListItem Text="18" Value="8"></asp:ListItem>
        <asp:ListItem Text="19" Value="8"></asp:ListItem>
        <asp:ListItem Text="20" Value="8"></asp:ListItem>
        <asp:ListItem Text="21" Value="8"></asp:ListItem>
        <asp:ListItem Text="22" Value="8"></asp:ListItem>
        <asp:ListItem Text="23" Value="8"></asp:ListItem>
        <asp:ListItem Text="24" Value="8"></asp:ListItem>
        <asp:ListItem Text="25" Value="8"></asp:ListItem>
        <asp:ListItem Text="26" Value="8"></asp:ListItem>
        <asp:ListItem Text="27" Value="8"></asp:ListItem>
        <asp:ListItem Text="28" Value="8"></asp:ListItem>
        <asp:ListItem Text="29" Value="8"></asp:ListItem>
        <asp:ListItem Text="30" Value="8"></asp:ListItem>
        <asp:ListItem Text="31" Value="8"></asp:ListItem>
        <asp:ListItem Text="32" Value="8"></asp:ListItem>
        <asp:ListItem Text="33" Value="8"></asp:ListItem>
        <asp:ListItem Text="34" Value="8"></asp:ListItem>
        <asp:ListItem Text="35" Value="8"></asp:ListItem>
        <asp:ListItem Text="36" Value="8"></asp:ListItem>
        <asp:ListItem Text="37" Value="8"></asp:ListItem>
        <asp:ListItem Text="38" Value="8"></asp:ListItem>
        <asp:ListItem Text="39" Value="8"></asp:ListItem>
        <asp:ListItem Text="40" Value="8"></asp:ListItem>
        <asp:ListItem Text="41" Value="8"></asp:ListItem>
        <asp:ListItem Text="42" Value="8"></asp:ListItem>
        <asp:ListItem Text="43" Value="8"></asp:ListItem>
        <asp:ListItem Text="44" Value="8"></asp:ListItem>
        <asp:ListItem Text="45" Value="8"></asp:ListItem>
        <asp:ListItem Text="46" Value="8"></asp:ListItem>
        <asp:ListItem Text="47" Value="8"></asp:ListItem>
        <asp:ListItem Text="48" Value="8"></asp:ListItem>
        <asp:ListItem Text="49" Value="8"></asp:ListItem>
        <asp:ListItem Text="50" Value="8"></asp:ListItem>
        <asp:ListItem Text="51" Value="8"></asp:ListItem>
        <asp:ListItem Text="52" Value="8"></asp:ListItem>
        </asp:DropDownList>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>Summary: </td>
    <td>
        <asp:TextBox ID="TextBox1" TextMode="MultiLine" Width="400px" BorderStyle="Ridge" Height="200px" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Select File To Import:
    </td>
    <td>
        <asp:FileUpload ID="FileUpload1" runat="server" BorderStyle="Ridge" />
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td colspan="2">
        <asp:Button ID="BtnSummarySheet" runat="server" Text="Generate Summary Sheet" onclick="Btn_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="BtnRegionWise" runat="server" Text="Generate Region Wise Sheet" 
            onclick="BtnRegionWise_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="BtnIndividual" runat="server" 
            Text="Generate Individual Wise Sheet" onclick="BtnIndividual_Click" />
    </td>
    </tr>    
    </table>

</asp:Content>

