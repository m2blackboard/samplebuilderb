﻿<%@ page language="C#" autoeventwireup="true" inherits="_Default, App_Web_z4pecfdp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="css/Sample.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <br />
    <table width="100%" border="0">
    <tr>
    <td colspan="2" style="text-align:center" class="MainHeader">
    Project Apple
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    <tr>
    <td style="width:75%;text-align:left">
    <table width="100%">
    <tr>
    <td class="clsSubHeader">
    All Respondents Must
    </td>
    </tr>
    <tr>
    <td>
    <br />
    </td>
    </tr>
    <tr>
    <td>
    Interviews to be conducted in JHB, PTA & CAPE TOWN
    </td>
    </tr>
    <tr>
    <td>
    Watch television regularly (i.e. at least 6 hrs per week)
    </td>
    </tr>
    <tr>
    <td>
    Drunk Juice in the last 7 days
    </td>
    </tr>
    <tr>
    <td>
    Be LSM A 7 - 10
    </td>
    </tr>
    <tr>
    <td>
    Be aged between 20 - 35 years old
    </td>
    </tr>
    <tr>
    <td>
    Be fully conversant in English
    </td>
    </tr>
    <tr>
    <td>
    No industry affiliation
    </td>
    </tr>
    </table>
    </td>
    <td style="width:25%;text-align:right" valign="top">
    <table width="100%" border="0">
    <tr>
    <td style="text-align:left" class="clsSubHeader">
    Project Details:
    </td>
    </tr>
    <tr>
    <td>
    <br />
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Interviewer:  
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Supervisor:
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Area:
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Rate:
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Job Number:
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    </table>
    <asp:Literal ID="litTest" runat="server"></asp:Literal>
    <br />
    <br />
   
    
    <table id="divReport" visible="false" width="1400px" cellspacing="0" cellpadding="0" border="1">
                        <tr>
                            <td style="text-align: center;background-color:red" colspan="21">
                                DUE IN RIVONIA TUESDAY 1 NOVEMBER 2011
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;width:20%" >
                                Name And Surname of Respondents
                            </td>
                            <td style="text-align: center;width:10%">
                                Respondent Telephone No's
                            </td>
                            <td style="text-align: center;width:4%">
                                Total
                            </td>
                            <td style="text-align: center;width:4%">
                                            Female
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            Male
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            25-34 years
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            35-40 years
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            Black
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            Coloured
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            Indian
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            White
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            LSM 7
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            LSM 8
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            LSM 9
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            LSM 10
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            Users have drunk Juice in the last 7 days
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            Axe Twist
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            McDonald
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            Jacobs krunung
                                        </td>
                                        <td style="text-align: center;width:4%">
                                            Ocean Basket
                                        </td>
                                        <td style="text-align: center;width:250px">
                                            Red Bull
                                        </td>
                        </tr>
                        </table>
    </div>
    </form>
</body>
</html>
