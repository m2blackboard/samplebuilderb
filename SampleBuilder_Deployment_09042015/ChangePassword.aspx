﻿<%@ page language="C#" masterpagefile="~/SampleMaster.master" autoeventwireup="true" inherits="ChangePassword, App_Web_dvy2uzvm" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/SampleMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="PageHeader">
        <span class="insetText"><span class="blueTITLEtext">&nbsp;Change Password</span></span></h1>
    <br />
    <asp:Panel ID="pnlPassword" Visible="true" runat="server">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="width:15%"><span class="clsMandatory">*</span>UserName
                </td>
                <td>
                <asp:TextBox ID="TxtUserName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtUserName" ErrorMessage="Enter Username" CssClass="clsMandatory">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr><td colspan="2"><br /></td></tr>
            <tr>
                <td><span class="clsMandatory">*</span>Current Password
                </td>
                <td>
                <asp:TextBox ID="TxtCurrent" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TxtCurrent" ErrorMessage="Enter Current Password"  CssClass="clsMandatory">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr><td colspan="2"><br /></td></tr>
            <tr>
                <td><span class="clsMandatory">*</span>New Password
                </td>
                <td>
                <asp:TextBox ID="TxtPassword" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TxtPassword" ErrorMessage="Enter New Password"  CssClass="clsMandatory">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr><td colspan="2"><br /></td></tr>
            <tr>
            <td>
            </td>
            <td><asp:Button ID="BtnPassword" Text="Change Password" runat="server" 
                    onclick="BtnPassword_Click" /></td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
