﻿<%@ page language="C#" masterpagefile="~/SampleMaster.master" autoeventwireup="true" inherits="AttendanceRegister, App_Web_dvy2uzvm" title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" >
    </asp:ScriptManager>
<br/>
    <h1 class="PageHeader">
       <span class="insetText"><span class="blueTITLEtext">&nbsp;Briefing Attendance Register</span></span></h1>
       <br />
       <table width="100%" border="0">
    <tr><td colspan="2" style="text-align:right" valign="top">
    <a id="lnkBack" style="text-align:right;vertical-align:top;float:left" runat="server" href="adminpage.aspx">Back</a><div><img src="Images/print4.gif" alt="" />
    <a id="lnkprint" style="vertical-align:top;" target="_blank" runat="server" href="#">Print</a>
    </div>
</td></tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    <tr><td class="clsSubText" style="font-size:medium;width:15%">Project: &nbsp;&nbsp;</td>
    <td><asp:Label ID="lblProject" runat="server" CssClass="clsText"  Font-Bold="true" ></asp:Label></td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Maconomy Number:</td>
    <td><asp:Label ID="lblMaconomyNumber" runat="server" CssClass="clsText"  Font-Bold="true"></asp:Label></td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Title Of Briefing: &nbsp;&nbsp;</td>
    <td><asp:Label ID="Label1" runat="server" CssClass="clsText" Text="Briefing / Re-briefing" Font-Bold="true"></asp:Label></td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Topic / Contents: &nbsp;&nbsp;</td>
    <td><asp:Label ID="Label2" runat="server" CssClass="clsText" Font-Bold="true" Text="Handing out of questionnaires/screeners and briefing documents, practical sessions, going through the script (if CAPI) and conducting mock interviews. "></asp:Label></td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Date: &nbsp;&nbsp;</td>
    <td><asp:TextBox ID="TxtDate" runat="server" ></asp:TextBox>
    <cc1:CalendarExtender ID="CalendarExtender1" Format="yyyy-MM-dd" TargetControlID="TxtDate" runat="server">
    </cc1:CalendarExtender>
    </td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Time / Duration: &nbsp;&nbsp;</td>
    <td><asp:TextBox ID="TxtTime" runat="server" ></asp:TextBox></td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Contact Person: &nbsp;&nbsp;</td>
    <td><asp:TextBox ID="TxtContactPerson" runat="server" Width="300px" ></asp:TextBox></td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Venue: &nbsp;&nbsp;</td>
    <td style="font-weight:bold">Millward Brown, Rivonia</td></tr>
    <tr><td><br /></td></tr>
    <tr><td class="clsSubText" style="font-size:medium;text-decoration:underline">Attendee:</td>
    <td><asp:Button ID="BtnUpdate" runat="server" onclick="BtnUpdate_Click" Text="Update" /></td></tr>
    <tr>
    <td colspan="2">
    <asp:Literal ID="litRegister" runat="server"></asp:Literal>
    </td>
    </tr>
    </table>
    <br />
    
    <table  cellspacing="0" cellpadding="0" style="width:100%" border="0">
            <tr>
            <td>
           
           <div style="padding-top:10px">_______________________________________________</div>
           <div style="padding-top:5px">
           <asp:Label ID="lblContactPerson" runat="server" ></asp:Label>
           <br />
           <div style="padding-top:5px">
           <asp:DropDownList ID="drpContact" runat="server" >
           <asp:ListItem Text="Project Assitant" Value="Project Assitant"></asp:ListItem>
           <asp:ListItem Text="Project Supervisor" Value="Project Supervisor"></asp:ListItem>
           <asp:ListItem Text="Project Controller" Value="Project Controller"></asp:ListItem>
           </asp:DropDownList>
           </div>
           </div>
            </td>
            <td style="width:10%;text-align:left">
            <div style="padding-top:0px;padding-bottom:5px">_______________________________________________</div>
            <asp:TextBox ID="TxtProjectManager" runat="server"></asp:TextBox>
            <div style="padding-top:5px">
            Project Manager</div>
            </td>
            </tr>
            </table>
            <br />
</asp:Content>

