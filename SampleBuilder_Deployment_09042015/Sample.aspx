﻿<%@ page language="C#" autoeventwireup="true" inherits="Sample, App_Web_z4pecfdp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td width="15%">
                    Project Name
                </td>
                <td>
                    <asp:TextBox ID="TxtProjectName" Width="300px" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Interviewer
                </td>
                <td>
                    <asp:TextBox ID="TxtInterviwer" Width="300px" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Supervisor
                </td>
                <td>
                    <asp:TextBox ID="TxtSupervisor" Width="300px" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Area
                </td>
                <td>
                    <asp:TextBox ID="TxtArea" Width="300px" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Rate
                </td>
                <td>
                    <asp:TextBox ID="TxtRate" Width="300px" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Job Number
                </td>
                <td>
                    <asp:TextBox ID="TxtJobNumber" Width="300px" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Contact Person
                </td>
                <td>
                    <asp:TextBox ID="TxtContactPerson" Width="300px" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    Select Sample File
                </td>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <asp:Literal ID="litText" runat="server"></asp:Literal>
                    <table width="100%" cellspacing="0" cellpadding="0" border="1">
                        <tr>
                            <td style="text-align: center" colspan="4">
                                DUE IN RIVONIA TUESDAY 1 NOVEMBER 2011
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center">
                                Name And Surname of Respondents
                            </td>
                            <td style="text-align: center">
                                Respondent Telephone No's
                            </td>
                            <td style="text-align: center">
                                Total
                            </td>
                            <td style="text-align: center">
                                <table width="100%" cellspacing="0" cellpadding="0" border="1">
                                    <tr>
                                        <td colspan="2">
                                            Gender
                                        </td>
                                        <td colspan="2">
                                            Age
                                        </td>
                                        <td colspan="4">
                                            Ethnic Group
                                        </td>
                                        <td colspan="4">
                                            LSM Grid
                                        </td>
                                        <td>
                                            Brand Usage
                                        </td>
                                        <td colspan="5">
                                            DVD Rotations
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Female
                                        </td>
                                        <td>
                                            Male
                                        </td>
                                        <td>
                                            25-34 years
                                        </td>
                                        <td>
                                            35-40 years
                                        </td>
                                        <td>
                                            Black
                                        </td>
                                        <td>
                                            Coloured
                                        </td>
                                        <td>
                                            Indian
                                        </td>
                                        <td>
                                            White
                                        </td>
                                        <td>
                                            LSM 7
                                        </td>
                                        <td>
                                            LSM 8
                                        </td>
                                        <td>
                                            LSM 9
                                        </td>
                                        <td>
                                            LSM 10
                                        </td>
                                        <td>
                                            Users have drunk Juice in the last 7 days
                                        </td>
                                        <td>
                                            Axe Twist
                                        </td>
                                        <td>
                                            McDonald
                                        </td>
                                        <td>
                                            Jacobs krunung
                                        </td>
                                        <td>
                                            Ocean Basket
                                        </td>
                                        <td>
                                            Red Bull
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                J1233 Laxmi Sanku
                            </td>
                            <td>
                                1
                            </td>
                            <td>
                                50
                            </td>
                            <td valign="top" style="height: 100%">
                                <table width="100%" style="height: 100%" border="1" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width: 6%">
                                            30
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                        <td>
                                            10
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <input type="submit" name="BtnGenerate" value="Generate Quota Sheet" id="Submit1" />
                    &nbsp;&nbsp;&nbsp;
                    <input type="submit" name="BtnCancel" value="Cancel" id="Submit2" />
                </td>
            </tr>
        </table>
        </td> </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="BtnGenerate" runat="server" Text="Generate Quota Sheet" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="BtnCancel" runat="server" Text="Cancel" />
            </td>
        </tr>
        </table>
    </div>
    </form>
</body>
</html>
