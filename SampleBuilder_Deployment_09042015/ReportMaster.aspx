﻿<%@ page language="C#" masterpagefile="~/SampleMaster.master" autoeventwireup="true" inherits="ReportMaster, App_Web_dvy2uzvm" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/SampleMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="PageHeader">
       <span class="insetText"> <span class="blueTITLEtext">&nbsp;Report Selection</span> </span></h1>
       <br />
       <br />
       <div style="float:left;width:10%">
       Select Project:</div>
       <div>
           <asp:DropDownList ID="DrpProject" runat="server" 
               onselectedindexchanged="DrpProject_SelectedIndexChanged">
           </asp:DropDownList>
       </div>
       <br />
       <div style="float:left;width:10%">
       Report Type:
       </div>
       <div>
           <asp:DropDownList ID="DrpReport" runat="server"  AutoPostBack="true"
               onselectedindexchanged="DrpReport_SelectedIndexChanged">
           <asp:ListItem Text="Select Report" Value="0"></asp:ListItem>
           <asp:ListItem Text="Summary Report" Value="1"></asp:ListItem>
           <asp:ListItem Text="Regional Report" Value="2"></asp:ListItem>
           <asp:ListItem Text="Individual Report" Value="3"></asp:ListItem>
           <asp:ListItem Text="Blank Report" Value="4"></asp:ListItem>
           </asp:DropDownList>
       </div>
       <br />
       <div id="divRegion" runat="server" visible="false">
       <div style="float:left;width:10%">
       Select Region
       </div>
       <div><asp:DropDownList ID="DrpLocation" runat="server" AutoPostBack="true"
               onselectedindexchanged="DrpLocation_SelectedIndexChanged">
           </asp:DropDownList></div>
           <br />
           </div>
      <div id="divInterviewers" runat="server" visible="false">
       <div style="float:left;width:10%">Select Individuals:</div>
        <div><asp:GridView ID="GdInterviewer" Width="400px"   AutoGenerateColumns="false" runat="server">
        <Columns>
        <asp:TemplateField>
        <HeaderStyle  BackColor="#CCCCFF" />
        <HeaderTemplate>
        <asp:CheckBox ID="ChkSelectAllInterviewer" AutoPostBack="true" OnCheckedChanged="ChkSelectAllInterviewer_CheckedChanged" runat="server" />
        </HeaderTemplate>
        <ItemTemplate>
        <asp:CheckBox id="chkSelInterviewer" runat="server" />
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-CssClass="clsLeft">
        <HeaderStyle  BackColor="#CCCCFF" />
        <HeaderTemplate >Interviewer Id</HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="LblInterviewerSysId" Text='<%# Bind("interviewersysid") %>'  runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-CssClass="clsLeft">
        <HeaderStyle  BackColor="#CCCCFF" />
        <HeaderTemplate >Select Interviewers</HeaderTemplate>
        <ItemTemplate>
        <asp:Label ID="LblInterviewerName" Text='<%# Bind("interviewername") %>'  runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-CssClass="clsLeft" Visible="false">
        <HeaderStyle  BackColor="#CCCCFF" />
        <ItemTemplate>
        <asp:Label ID="LblInterviewerId" Text='<%# Bind("INTERVIEWERID") %>' Visible="true" runat="server"></asp:Label>
        </ItemTemplate>
        </asp:TemplateField>
        </Columns>
        </asp:GridView></div>
        </div>
        <br />
        <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
        <asp:Button ID="btnReport" runat="server" Text="View Report" 
        onclick="btnReport_Click" />
        <br />
        <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
</asp:Content>

