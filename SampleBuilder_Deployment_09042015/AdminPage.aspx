﻿<%@ page language="C#" masterpagefile="~/SampleMaster.master" autoeventwireup="true" inherits="AdminPage, App_Web_dvy2uzvm" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/SampleMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h1 class="PageHeader">
       <span class="insetText"> <span class="blueTITLEtext">&nbsp;Admin Docs</span> </span></h1>
       <br />
       <br />
       <div style="float:left;width:10%">
       Select Project:</div>
       <div>
           <asp:DropDownList ID="DrpProject" runat="server" >
           </asp:DropDownList>
       </div>
       <br />
       <div style="float:left;width:10%">
       Document Type:
       </div>
       <div>
           <asp:DropDownList ID="DrpReport" runat="server"  AutoPostBack="true">
           <asp:ListItem Text="Select Doc Type" Value="0"></asp:ListItem>
           <asp:ListItem Text="Project Summary Report" Value="1"></asp:ListItem>
           <asp:ListItem Text="Briefing Attendance Register" Value="2"></asp:ListItem>
           <asp:ListItem Text="Packing Slip" Value="3"></asp:ListItem>
           </asp:DropDownList>
       </div>
       <br />
        <br />
        <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
        <asp:Button ID="btnReport" runat="server" Text="View Report" 
        onclick="btnReport_Click" />
        <br />
        <div style="margin-top:10px;border:1px solid gray;margin-bottom:10px"></div>
</asp:Content>

