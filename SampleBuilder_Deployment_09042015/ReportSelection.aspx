﻿<%@ page language="C#" masterpagefile="~/SampleMaster.master" autoeventwireup="true" inherits="ReportSelection, App_Web_dvy2uzvm" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br/>
<br/>
    <h1 class="PageHeader">
       <span class="insetText"> <span class="blueTITLEtext">&nbsp;Report Selection</span> </span></h1>
       <br />
       <br />
       <table width="100%" cellpadding="0" cellspacing="0" border="0">
       <tr>
       <td style="width:10%">
       Select Project:
       </td>
       <td>
           <asp:DropDownList ID="DrpProject" runat="server" Width="200px" >
           </asp:DropDownList>
       </td>
       </tr>
       <tr>
       <td colspan="2">
       <br />
       </td>
       </tr>
       <tr>
       <td style="width:10%">
       Select Region:
       </td>
       <td>
           <asp:DropDownList ID="DrpRegion" runat="server" AutoPostBack="true" Width="200px"
               onselectedindexchanged="DrpRegion_SelectedIndexChanged">
           </asp:DropDownList>
       </td>
       </tr>
       <tr>
       <td colspan="2">
       <br />
       </td>
       </tr>
       <tr>
       <td style="width:10%">
       Select Individual:
       </td>
       <td>
           <asp:DropDownList ID="DrpIndividual" runat="server" Width="200px">
           <asp:ListItem  Text="Select Interviewer" Value="Select">
           </asp:ListItem>
           
           </asp:DropDownList>
       </td>
       </tr>
       <tr>
       <td colspan="2"><br /></td>
       </tr>
       <tr>
       <td colspan="2">
           <asp:Button ID="BtnReport" runat="server" Text="Generate Report" 
               onclick="BtnReport_Click" />
       </td>
       </tr>
       </table>
</asp:Content>

