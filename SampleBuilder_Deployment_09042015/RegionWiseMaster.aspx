﻿<%@ page language="C#" masterpagefile="~/SampleMaster.master" autoeventwireup="true" inherits="RegionWiseMaster, App_Web_dvy2uzvm" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/SampleMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br/>
<br/>
    <h1 class="PageHeader">
       <span class="insetText"><span class="blueTITLEtext">&nbsp;<asp:Literal ID="litProjectName" runat="server"></asp:Literal></span> </span></h1>
       <br />
       <table width="100%" border="0">
    <tr><td colspan="2" style="text-align:right" valign="top">
    <a id="lnkBack" style="text-align:right;vertical-align:top;float:left" runat="server" href="Reportmaster.aspx?bk=true">Back</a><div><img src="Images/print4.gif" alt="" />
                            <a id="lnkprint" style="vertical-align:top;" target="_blank" runat="server" href="#">Print</a>
                            <%--<img src="Images/Export2.jpg" alt="" />
                            <a href="#" id="lnkExport" style="vertical-align:top;" runat="server" enableviewstate="false" class="clsSmallColorText" >Export</a>--%>
                            </div>
</td></tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    <tr>
    <td style="width:40%;text-align:left;vertical-align:top">
    <asp:Literal ID="litNotes" runat="server"></asp:Literal>
    </td>
    <td style="width:50%;text-align:right" valign="top">
    <table width="100%" border="0">
    <tr>
    <td style="text-align:left;line-height:40px;" class="clsSubText">
    Project Details:
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Interviewer:&nbsp;&nbsp;&nbsp;<asp:Literal ID="litInterviewer" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Supervisor:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitSupervisor" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Area:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitArea" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Rate:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitRate" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Job Number:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitJobNumber" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Contact Person:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitContactPerson" runat="server"></asp:Literal>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    </table>
    <br />
    <asp:Literal ID="litReport" runat="server"></asp:Literal>
    <br />
</asp:Content>

