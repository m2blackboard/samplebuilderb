﻿<%@ page language="C#" autoeventwireup="true" inherits="ProjectSummaryReportPrint, App_Web_z4pecfdp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="css/Sample.css?v=1" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <br/>
    <h1 class="PageHeader">
       <span class="insetText"><span class="blueTITLEtext">&nbsp;Project Summary Report</span></span></h1>
       <br />
       <table width="100%" border="0">
<%--    <tr><td colspan="2" style="text-align:right" valign="top">
    <a id="lnkBack" style="text-align:right;vertical-align:top;float:left" runat="server" href="Reportmaster.aspx?bk=true">Back</a><div><img src="Images/print4.gif" alt="" />
    <a id="lnkprint" style="vertical-align:top;" target="_blank" runat="server" href="#">Print</a>
    </div>
</td></tr>--%>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    <tr><td class="clsSubText" style="font-size:medium">Project: &nbsp;&nbsp;<asp:Label ID="lblProject" runat="server"></asp:Label></td>
    <td class="clsSubText" style="text-align:right;font-size:medium">Maconomy Number: &nbsp;&nbsp;<asp:Label ID="lblMaconomyNumber" runat="server"></asp:Label></td>
    </tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    </table>
    <asp:Literal ID="litProjectSummary" runat="server"></asp:Literal>
    <br />
    </div>
    <div style="bottom:0;position:fixed;text-align:center;width:100%">
    <asp:Literal ID="litFooter" runat="server"></asp:Literal>
    </div>
    </form>
</body>
</html>
