﻿<%@ page language="C#" masterpagefile="~/SampleMaster.master" autoeventwireup="true" inherits="PackingSlip, App_Web_dvy2uzvm" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/SampleMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br/>
    <h1 class="PageHeader">
       <span class="insetText"><span class="blueTITLEtext">&nbsp;Packing Slip</span></span></h1>
       <br />
       <table width="100%" border="0">
    <tr><td colspan="2" style="text-align:right" valign="top">
    <a id="lnkBack" style="text-align:right;vertical-align:top;float:left" runat="server" href="adminpage.aspx">Back</a><div><img src="Images/print4.gif" alt="" />
    <a id="lnkprint" style="vertical-align:top;" target="_blank" runat="server" href="#">Print</a>
    </div>
</td></tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    <tr><td class="clsSubText" style="font-size:medium">Project: &nbsp;&nbsp;<asp:Label ID="lblProject" runat="server"></asp:Label></td>
    <td class="clsSubText" style="text-align:right;font-size:medium">Maconomy Number: &nbsp;&nbsp;<asp:Label ID="lblMaconomyNumber" runat="server"></asp:Label></td>
    </tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    </table>
    <asp:Label ID="lblSlip" Text="Packing SLip" runat="server" CssClass="clsGDheader" Visible="false" Width="100%"></asp:Label>
    <asp:GridView ID="GDTest" runat="server" AutoGenerateColumns="false"  Width="100%">
    <Columns>
    <asp:TemplateField>
    <HeaderStyle  BackColor="#CCCCFF" Width="200px" />
    <HeaderTemplate><asp:Label ID="lbl1" runat="server" Text="Name of Respondents"></asp:Label> </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblnameof" runat="server" Text='<%# Bind("NameOfInterviewer") %>'  ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderStyle  BackColor="#CCCCFF" />
    <HeaderTemplate><asp:Label ID="lbl1" runat="server" Text="Total Quesstionaires"></asp:Label> </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblTotalQues" runat="server" Text='<%# Bind("TotalQuesstionaires") %>' ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderStyle  BackColor="#CCCCFF" />
    <HeaderTemplate><asp:Label ID="lbl1" runat="server" Text=" Sample "></asp:Label> </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblsample" runat="server" Text='<%# Bind("Sample") %>' ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderStyle  BackColor="#CCCCFF" />
    <HeaderTemplate><asp:Label ID="lbl1" runat="server" Text="Eng/Afri Q'nars"></asp:Label> </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblEngAfriQnars" runat="server" Text='<%# Bind("EngAfriQnars") %>' ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderStyle  BackColor="#CCCCFF" Width="80px" />
    <HeaderTemplate><asp:Label ID="lbl1" runat="server" Text="Other Q'nars"></asp:Label> </HeaderTemplate>
    <ItemTemplate>
    <asp:TextBox ID="txtOtherQnars" runat="server" Text="" Width="80px"></asp:TextBox>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderStyle  BackColor="#CCCCFF" />
    <HeaderTemplate><asp:Label ID="lbl1test" runat="server" Text="Extra Eng/Afr Q'nars"></asp:Label> </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblExtrsEngAfriQnars" runat="server" Text='<%# Bind("ExtraEngAfriQnars") %>' ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderStyle  BackColor="#CCCCFF" Width="120px" />
    <HeaderTemplate><asp:Label ID="lbl1" runat="server" Text="Extra Other Q'nars"></asp:Label> </HeaderTemplate>
    <ItemTemplate>
    <asp:TextBox ID="TxtExtraOther" runat="server" Text="" Width="120px"></asp:TextBox>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderStyle  BackColor="#CCCCFF" />
    <HeaderTemplate><asp:Label ID="lbl1" runat="server" Text="ShowCards"></asp:Label> </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblshowcard" runat="server" Text='<%# Bind("ShowCards") %>' ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderStyle  BackColor="#CCCCFF" />
    <HeaderTemplate><asp:Label ID="lbl1" runat="server" Text="Briefing Instruction"></asp:Label> </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblbrief" runat="server" Text='<%# Bind("BriefingInstruction") %>' ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderStyle  BackColor="#CCCCFF" />
    <HeaderTemplate><asp:Label ID="lbl1" runat="server" Text="Blank Quotas"></asp:Label> </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblblank" runat="server" Text='<%# Bind("BlankQuotas") %>' ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
    <HeaderStyle  BackColor="#CCCCFF" />
    <HeaderTemplate><asp:Label ID="lbl1" runat="server" Text="Change Log"></asp:Label> </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="lblchange" runat="server" Text='<%# Bind("ChangeLog") %>' ></asp:Label>
    </ItemTemplate>
    </asp:TemplateField>
    </Columns>
    </asp:GridView>
    <%--<asp:GridView ID="GDPackingSlip" runat="server" AutoGenerateColumns="false" Width="100%"
        OnRowEditing ="GDPackingSlip_RowEditing" 
        OnRowCancelingEdit="GDPackingSlip_RowCancelingEdit" 
        OnRowUpdating="GDPackingSlip_RowUpdating" OnRowDataBound="GDPackingSlip_RowDataBound" >
      <Columns>
            <asp:CommandField ShowEditButton="True">
                <ItemStyle Width="25px" />
            </asp:CommandField>
            </Columns>
    </asp:GridView>--%>
    <br />
    <br />
    <asp:Button Text="Update" ID="BtnUpdate" runat="server" 
        onclick="BtnUpdate_Click" Height="50px" Width= "75px" />
</asp:Content>

