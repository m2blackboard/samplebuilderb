﻿<%@ page language="C#" autoeventwireup="true" inherits="Project, App_Web_z4pecfdp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div >
    <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td style="width:15%">
    Project Name:
    </td>
    <td>
        <asp:TextBox ID="TxtProject" Width="300px" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Interviewer:
    </td>
    <td>
        <asp:TextBox ID="TxtInterviewer"  Width="300px" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Supervisor:
    </td>
    <td>
        <asp:TextBox ID="TxtSupervisor" Width="300px" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Area:
    </td>
    <td>
        <asp:TextBox ID="TxtArea" Width="300px" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Rate:
    </td>
    <td>
        <asp:TextBox ID="TxtRate" Width="300px" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Job Number:
    </td>
    <td>
        <asp:TextBox ID="TxtJobNumber" Width="300px" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>Summary</td>
    <td>
        <asp:TextBox ID="TextBox1" TextMode="MultiLine" Width="400px" Height="200px" runat="server"></asp:TextBox>
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td>
    Select File To Export data:
    </td>
    <td>
        <asp:FileUpload ID="FileUpload1" runat="server" />
    </td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td colspan="2"><br /></td>
    </tr>
    <tr>
    <td colspan="2">
        <asp:Button ID="Btn" runat="server" Text="Generate Summary Sheet" 
            onclick="Btn_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button1" runat="server" Text="Generate Region Wise Sheet" 
            onclick="Button1_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="Button2" runat="server" Text="Generate Individual Wise Sheet" 
            onclick="Button2_Click" />
    </td>
    </tr>    
    </table>
    </div>
    </form>
</body>
</html>
