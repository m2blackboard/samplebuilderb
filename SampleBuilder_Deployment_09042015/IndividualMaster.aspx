﻿<%@ page language="C#" masterpagefile="~/SampleMaster.master" autoeventwireup="true" enableeventvalidation="false" inherits="IndividualMaster, App_Web_dvy2uzvm" title="Untitled Page" aspcompat="true" %>
<%@ MasterType VirtualPath="~/SampleMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">

    //$(document).ready(function () {
        //document.getElementById("divMainPage").class="pageLarge";
    //    debugger
        //var divmain= document.getElementById("divMainPage");
//        document.getElementById("divMainPage").className="pageLarge";
  //      var div1;
    //});
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:Literal ID="litScript" runat="server"></asp:Literal>
<br/>
<br/>
    <h1 class="PageHeader">
       <span class="insetText"> <span class="blueTITLEtext">&nbsp;<asp:Literal ID="litProjectName" runat="server"></asp:Literal></span></span></h1>
       <br />
       <table width="100%" border="0">
    <%--<tr>
    <td colspan="2">
    <br />
    </td>
    </tr>--%>
    <tr><td colspan="2" style="text-align:right" valign="top">
    <div style="width:30px;float:left"><a id="lnkBack" style="text-align:right;vertical-align:top;float:left" runat="server" href="Reportmaster.aspx?bk=true">Back</a>&nbsp;&nbsp;&nbsp;</div>
    <%--<div style="width:100px;float:left"><a id="lnkLock" runat="server" href="#">
    UnLockProject</a></div>--%>
    <div><img src="Images/print4.gif" alt="" />
                            <a id="lnkprint" style="vertical-align:top;" target="_blank" runat="server" href="#">Print</a>
                            <%--<img src="Images/Export2.jpg" alt="" />
                            <a href="#" id="lnkExport" style="vertical-align:top;" runat="server" enableviewstate="false" class="clsSmallColorText" >Export</a>--%>
                            </div>
</td></tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    <tr>
    <td style="width:55%;text-align:left;vertical-align:top">
        <asp:Literal ID="litNotes" runat="server"></asp:Literal>
    </td>
    <td style="width:50%;text-align:left" valign="top">
    <table width="100%" border="0">
    <tr>
    <td style="text-align:left;line-height:40px;" class="clsSubText">
    Project Details:
    </td>
    </tr>
    <%--<tr>
    <td>
    <br />
    </td>
    </tr>--%>
    <tr>
    <td style="text-align:left">
    Interviewer:&nbsp;&nbsp;&nbsp;<asp:Literal ID="litInterviewer" runat="server"></asp:Literal>  
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Supervisor:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitSupervisor" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Area:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitArea" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Rate:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitRate" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Job Number:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitJobNumber" runat="server"></asp:Literal>
    </td>
    </tr>
    <tr>
    <td style="text-align:left">
    Contact Person:&nbsp;&nbsp;&nbsp;<asp:Literal ID="LitContactPerson" runat="server"></asp:Literal>
    </td>
    </tr>
    </table>
    </td>
    </tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    </table>
    <div style="color:#0078ad;font-size:small;font-weight:bold;padding-bottom:10px;">
    Briefing into field: <asp:Literal ID="litBriefDate" runat="server" ></asp:Literal>
    </div>
    <div style="color:#0078ad;font-size:small;font-weight:bold;padding-bottom:10px;">
    Due in Rivonia Area: <asp:Literal ID="LitDueInRivoniaArea" runat="server" ></asp:Literal>
    </div>
    <div style="color:#0078ad;font-size:small;font-weight:bold;padding-bottom:10px;">
    Due in Rivonia Locals: <asp:Literal ID="LitDueInRivoniaLocal" runat="server" ></asp:Literal>
    </div>
    <asp:Literal ID="litIndividual" runat="server"></asp:Literal>
    <%--<asp:GridView ID="gdTest" runat="server" OnRowEditing="gdTest_RowEditing"  
        OnRowCancelingEdit="gdTest_RowCancelingEdit" OnRowCreated="gdTest_RowCreated" 
        OnRowDataBound="gdTest_RowDataBound" OnRowUpdating="gdTest_RowUpdating">
        <Columns>
            <asp:CommandField ShowEditButton="True">
                <ItemStyle Width="25px" />
            </asp:CommandField>
        </Columns>
        <EditRowStyle Width="10"/>
    </asp:GridView>
    <br />--%>
    <br />
        <asp:GridView ID="TableGridView" runat="server" AutoGenerateColumns="false" Width="100%"
        OnRowEditing ="TableGridView_RowEditing" 
        OnRowCancelingEdit="TableGridView_RowCancelingEdit" 
        OnRowUpdating="TableGridView_RowUpdating" OnRowDataBound="TableGridView_RowDataBound" 
      >
      <Columns>
            <asp:CommandField ShowEditButton="True">
                <ItemStyle Width="25px" />
            </asp:CommandField>
            </Columns>
    </asp:GridView>
    <%--<asp:Button ID="BTNTEST" runat="server" Text="TST" onclick="BTNTEST_Click" />--%>
</asp:Content>

