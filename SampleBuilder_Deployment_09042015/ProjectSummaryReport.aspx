﻿<%@ page language="C#" masterpagefile="~/SampleMaster.master" autoeventwireup="true" inherits="ProjectSummaryReport, App_Web_dvy2uzvm" title="Untitled Page" %>
<%@ MasterType VirtualPath="~/SampleMaster.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br/>
    <h1 class="PageHeader">
       <span class="insetText"><span class="blueTITLEtext">&nbsp;Project Summary Report</span></span></h1>
       <br />
       <table width="100%" border="0">
    <tr><td colspan="2" style="text-align:right" valign="top">
    <a id="lnkBack" style="text-align:right;vertical-align:top;float:left" runat="server" href="adminpage.aspx">Back</a><div><img src="Images/print4.gif" alt="" />
    <a id="lnkprint" style="vertical-align:top;" target="_blank" runat="server" href="#">Print</a>
    </div>
</td></tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    <tr><td class="clsSubText" style="font-size:medium">Project: &nbsp;&nbsp;<asp:Label ID="lblProject" runat="server"></asp:Label></td>
    <td class="clsSubText" style="text-align:right;font-size:medium">Maconomy Number: &nbsp;&nbsp;<asp:Label ID="lblMaconomyNumber" runat="server"></asp:Label></td>
    </tr>
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    </table>
    <asp:Literal ID="litProjectSummary" runat="server"></asp:Literal>
</asp:Content>

