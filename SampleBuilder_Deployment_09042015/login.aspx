﻿<%@ page language="C#" autoeventwireup="true" inherits="login, App_Web_z4pecfdp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link href="css/Site.css?v=1" rel="stylesheet" type="text/css" />
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server" color="black">
    <div class="page">
        <div class="header" >
            <div class="title">
                <h1 >
                    <span class="blueTITLEtext">S</span><span class="greyTITLEtext">ample</span> <span
                        class="blueTITLEtext">Builder </span><span class="greyTITLEtext"></span>
                </h1>
            </div>
            <div class="loginDisplay">
           <table align="right">
           <tr>
           <td>
               &nbsp;</td>
           </tr>
             <tr>
           <td>
               &nbsp;</td>
           </tr>
           </table>
            </div>
           
        </div>
        <br/>
        <div class="main">
                <asp:Panel ID="pnlLogin" runat="server">
    <h1 class="PageHeader">
       <span class="insetText">   <span class="blueTITLEtext">Log in&nbsp; </span></span>
    </h1>
       </asp:Panel>
    <br />
    <br />
    <div id="divlogout" runat="server" visible="false" class="blueTITLEtext" style="text-align:center">You have successfully logged out of the system.</div>
     <div>
        <table align="center">
        <tr><td><asp:Label ID="lblMsg" Visible="false" runat="server" CssClass="lblMessage"></asp:Label></td></tr>
        <tr>
        <td><br /></td>
        </tr>
        <tr>
        <td align="center">
            <img src="Images/Login.jpg" style="width: 124px; height: 90px" />
        </td>
        </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblUserName" runat="server" Text="User Name"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtUserName" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtUserName" ErrorMessage="Enter UserName">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="txtPassword" ErrorMessage="Enter Password">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnLogin" runat="server" CssClass="submitButton" Text="Log In" 
                        text-align="center" Width="52px" onclick="btnLogin_Click" />
                </td>
            </tr>
        </table>
 
    </div></div>
        <div class="clear">
        </div>
    </div>
    </form>
</body>
</html>
