﻿<%@ page language="C#" autoeventwireup="true" inherits="AttendanceRegisterPrint, App_Web_z4pecfdp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="css/Sample.css?v=1" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
<br/>
    <h1 class="PageHeader">
       <span class="insetText"><span class="blueTITLEtext">&nbsp;Briefing Attendance Register</span></span></h1>
       <br />
       <table width="100%" border="0">
    <tr>
    <td colspan="2">
    <br />
    </td>
    </tr>
    <tr><td class="clsSubText" style="font-size:medium;width:15%">Project: &nbsp;&nbsp;</td>
    <td><asp:Label ID="lblProject" runat="server" CssClass="clsText"  Font-Bold="true" ></asp:Label></td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Maconomy Number:</td>
    <td><asp:Label ID="lblMaconomyNumber" runat="server" CssClass="clsText"  Font-Bold="true"></asp:Label></td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Title Of Briefing: &nbsp;&nbsp;</td>
    <td><asp:Label ID="Label1" runat="server" CssClass="clsText" Text="Briefing / Re-briefing" Font-Bold="true"></asp:Label></td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Topic / Contents: &nbsp;&nbsp;</td>
    <td><asp:Label ID="Label2" runat="server" CssClass="clsText" Font-Bold="true" Text="Handing out of questionnaires/screeners and briefing documents, practical sessions, going through the script (if CAPI) and conducting mock interviews. "></asp:Label></td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Date: &nbsp;&nbsp;</td>
    <td><asp:Label ID="lblDate" runat="server" Font-Bold="true"></asp:Label>
    </td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Time / Duration: &nbsp;&nbsp;</td>
    <td><asp:Label ID="lblTime" runat="server" Font-Bold="true"></asp:Label></td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Contact Person: &nbsp;&nbsp;</td>
    <td><asp:Label ID="lblContactpersonTitle" runat="server" Font-Bold="true"></asp:Label></td></tr>
    <tr><td class="clsSubText" style="font-size:medium">Venue: &nbsp;&nbsp;</td>
    <td style="font-weight:bold">Millward Brown, Rivonia</td></tr>
    <tr><td><br /></td></tr>
    <tr><td class="clsSubText" style="font-size:medium;text-decoration:underline">Attendee:</td>
    <td></td></tr>
    <tr>
    <td colspan="2">
    <asp:Literal ID="litRegister" runat="server"></asp:Literal>
    </td>
    </tr>
    </table>
    <br />
    <br />
    <table  cellspacing="0" cellpadding="0" style="width:100%" border="0">
            <tr>
            <td>
          
           <div style="padding-top:10px">_______________________________________________</div>
            
           <div style="padding-top:5px">
           <asp:Label ID="lblContactPerson" runat="server" ></asp:Label>
           <div style="padding-top:5px">
           <asp:Label ID="lblContactPersonBelow" runat="server" Font-Bold="true"></asp:Label>
           </div>
           </div>
            </td>
            <td style="width:10%;text-align:left">
            <div >
            <div style="padding-top:0px;padding-bottom:5px">_______________________________________________</div>
            <asp:Label ID="lblProjectManager" runat="server" ></asp:Label>
            <div style="padding-top:5px;font-weight:bold">
            Project Manager</div>
            </div>
            </td>
            </tr>
            </table>
            <br />    
    </div>
    <div style="bottom:0;position:fixed;text-align:center;width:100%">
    <asp:Literal ID="litFooter" runat="server"></asp:Literal>
    </div>
    </form>
</body>
</html>
